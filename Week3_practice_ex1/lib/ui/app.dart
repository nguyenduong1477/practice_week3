import 'package:flutter/material.dart';
import '../stream/text_stream.dart';

class App extends StatefulWidget
{
  State<StatefulWidget> createState()
  {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>
{
  String text = 'welcome';
  TextStream textStream = TextStream();
  late int count = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Text',

      home: Scaffold
        (
          appBar: AppBar(title: Text('App Bar'),),
          body: Center(
              child:Column(
                  children:[
                    Text(text, style: TextStyle(fontSize: 50))

                  ]

              )
          ),

          floatingActionButton: FloatingActionButton
            (
              child: Text('C'),
              onPressed: (){
                changeText();
              },
            ),
        )
    );
  }

  changeText() async
  {
    textStream.getTexts().listen((eventText) {
      setState(() {
        print(eventText);
        text = eventText.toUpperCase();
        if (count == eventText.length+1)
          count = 1;
        else count +=1;
      });
    });
  }
}