import 'dart:html';

import 'package:flutter/material.dart';

class TextStream
{

  Stream<String> getTexts() async*
  {
    final List<String> texts =
    [
      'Welcome',
      'To',
      'Text',
      'Stream',
      'Flutter',
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t)
    {
      int index = t % 5;
      return texts[index];
    });
  }
}